# Map Reduce Assignment
---
## Alikhan Bulatov
### Student ID: 991 465 626
---
#### Language Choice: python for both (jupyter notebooks)
#### Distributed Solution: multiproccesor approach 
---
### PROGRAM OUTPUTS:
![picture](../../downloads/seq.png)
![picture](../../downloads/par.png)
---
#### Parallel map reduce was always ˜2 times faster than the sequential approach
